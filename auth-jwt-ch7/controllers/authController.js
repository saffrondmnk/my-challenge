const { User } = require("../models");
const { Rooms } = require("../models");
const { Results } = require("../models");

// const passport = require("../lib/passport");



const register = (req, res) => {
    User.register(req.body)
    .then(() => res.redirect("/users/login"))
    .catch(err => {
        console.log(err);
        next(err);
        
    });
     
}

const registerForm = (req, res, next) => {
    res.render("register");
}

// const login = () => {
//     passport.authenticate("local", {
//         successRedirect: "/",
//         failureRedirect: "/users/login",
//         failureFlash: true,

//     })
// }

const login = (req, res, next) => {
    User.authenticate(req.body)
    .then(user => {
        res.json({
            id: user.id,
            username: user.username,
            accessToken: user.generateToken()
        });
    })
    .catch(err => {
        console.log(err);
        next(err);
    });
}

const rooms = (req, res, next) => {
    Rooms.findOne( {
        where: {id: req.params.id}
    }).then(rooms => {
        res.render("/rooms/:id/fight");
    })

    rooms.authenticate(req.body)
    .then(rooms => {
        res.json( {
            status: "",
        })
    })

}

const result = (req, res, next) => {
    res.render('results');

}

// const login = passport.authenticate("local", {
//         successRedirect: "/",
//         failureRedirect: "/users/login",
//         failureFlash: true,

//     });


const loginForm = (req, res, next) => {
    res.render("login");
}


const whoami = (req, res, next) => {
    // res.render("profile", req.user.dataValues);
    const currentUser = req.user;
    res.json({
        id: currentUser.id,
        username: currentUser.username,
     });
}
module.exports = {
    register,
    registerForm,
    login,
    loginForm,
    whoami,
    rooms,
    results,
};
const passport = require("passport");
const { Strategy : JwtStrategy, ExtractJwt } = require("passport-jwt");
const { User } = require("../models");
const options = {
    jwtFromRequest: ExtractJwt.fromHeader("authorization"),
    secretOrKey: process.env.SECRET, 
}; 

passport.use(new JwtStrategy(options, async (payload, done) => { 
    User.findByPk(payload.id)
    .then(user => done(null, user))
    .catch(err => done(err, false));
})); 

module.exports = passport;















// const passport = require("passport");
// const localStrategy = require("passport-local").Strategy;
// const { User } = require("../models");

// const authenticate = async (username, password, done) => {
//     try {
//         const user = await User.authenticate({ username, password});

//     return done(null, user);
//     } catch (error) {
//         return done (null, false, { message: error });
//     }
// }
    
    

//  passport.use(
//     new localStrategy({
//         usernameField: "username",
//         passwordField: "password",
//     }, authenticate)
//  );
    


// passport.serializeUser((user, done) => done(null, user.id));

// passport.deserializeUser(async (id, done) => done(null, await User.findByPk(id)));


// module.exports = passport;
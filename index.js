const fs = require("fs");
const express = require("express");
const app = express();
const port = 8080;
const users = [];
// const router = require("./user");

// ############## ORM ######################

const { Article } = require("./models");
const { UserGame } = require("./models");
const { UserGameBiodata } = require("./models");
const { UserGameHistory } = require("./models");

// #########################################

// let isLogin = false ;


// app.use('/assets', express.static(__dirname + '/assets'));
app.use(express.static(__dirname + '/'));

app.use(express.json());
app.use(
    express.urlencoded({
        extended: false
    })
);
app.set("view engine", "ejs");

app.post("/register", (req, res) => {
    const { username, password, name} = req.body;
    if (!username || !password || !name) {
        res.sendStatus(400);
        return;
    }

    const existingUser = users.find(user => user.username === username);

    if (existingUser) {
        res.sendStatus(409);
        return;
    }

    const user = {
        username,
        password,
        name,
    };

    users.push(user);
    fs.writeFileSync("./users.json", JSON.stringify(users));
    res.sendStatus(200);

});


app.post("/login", (req, res) => {
    const { username, password } = req.body;
   

    if (!username || !password) {
        res.sendStatus(400);
        return;
    }
 
    const existingUser = users.find(user => user.username === username);
 
    if (!existingUser || existingUser.password !== password) {
        res.sendStatus(401);
        return;
    }

    currentUser.push(user);
    fs.writeFileSync("./current-user.json", JSON.stringify(currentUser));
    res.status(200).json({
        name: existingUser.name,
    })
});

// app.post ("/logout", (req, res) => {
//     if(!currentUser.username) {
//         res.sendStatus(401);
//     } else {
//         currentUser = {};
//         fs.writeFileSync("./current-user.json", JSON.stringify(currentUser));
//     }
// });

// app.get("/", (req, res) => {
//     res.send("Hello World");
// });

app.get("/index.html", (req, res) => {
    // res.sendFile("C:\\Users\\SaffronDamanik\\Documents\\challenge ver2\\kolab\\my-challenge\\index.html");
    res.sendFile("./index.html", {root: __dirname});
});

app.get("/game.html", (req, res) => {
    // res.sendFile("C:\\Users\\SaffronDamanik\\Documents\\challenge ver2\\kolab\\my-challenge\\index.html");
    res.sendFile("./game.html", {root: __dirname});
});

// app.post("/users", (req, res) => {
    
//     res.json({
//         name : req.query.name,
//         email : req.query.email,
//         password: req.query.password,
//     });
// });


// ############################# ORM ###############################

app.get("/articles", (req, res) => {
    Article.findAll().then(articles => {
        res.status(200).json(articles)
    });
});


app.post("/articles", (req, res) => {
    const { title, body, approved} = req.body;

    if (!title || !body) {
        res.sendStatus(400);
        return
    }
    Article.create({
        title, 
        body, 
        approved
    }).then(article => {
        res.status(200).json(article)
    }).catch(err => {
        res.sendStatus(500);
    });
    
});

// #####################################################

// ################### CH6 ##############################


app.get("/create", (req, res) => {
    res.render("create")
});



app.post("/create", (req, res) => {

const {usergamebiodata} = req.body;
    UserGame.create( {
       
        username: req.body.username,
        password: req.body.password,

        usergamebiodata: {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            birthday: req.body.birthday,
            gender: req.body.gender,
            location: req.body.location,
            phone: req.body.phone,
        }

    }, {
        include: [{
            model: userGameBiodata,
            as: "userGameBiodata"
        }]
    }).then(usergame => {
        console.log(usergame);
        res.status(200).json(usergame);
    });

});

// app.get("/users/", (req, res) => {

// }); 

// app.put("/users/", (req, res) => {

// });

// app.delete("/users/", (req, res) => {

// });


// ################################## END ORM ############################################



// app.use(router);
app.listen(port, ()  =>  
    console.log(`Server is running at http://localhost:${port}`)
);
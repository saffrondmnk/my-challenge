var express = require('express');
var router = express.Router();

const auth = require("../controllers/authController");
const restrict = require("../middlewares/restrict");


/* GET home page. */
router.get('/', restrict, function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/rooms', restrict, function(req, res, next) {
  res.render('rooms',);
});

router.post("/rooms/:id/fight", auth.rooms);

router.get("/results", auth.results);

module.exports = router;

'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      player_1_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      player_2_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      player_1_hands: {
        type: Sequelize.ARRAY,
        allowNull: false,
      },
      player_2_hands: {
        type: Sequelize.ARRAY,
        allowNull: false,
      },
      results: {
        type: Sequelize.ARRAY
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Rooms');
  }
};